%include "lib.inc"
global find_word

section .text

find_word:
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi
    .loop:
        mov rdi, r12
        mov rsi, [r13+8]
        call string_equals
        test rax, rax
        jnz .ret
        mov r13, [r13]
        test r13, r13
        jz .ret
        jmp .loop
    .ret:
        mov rax, r13
        pop r13
        pop r12
        ret
