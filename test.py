from subprocess import Popen, PIPE
input_words = ["first","second","third","a"*260,"qq"]
results = ["first word explanation", "second word explanation", "third word explanation","input value length is up to 256","value not found in dictionar"]
for i in range(len(input_words)):
	res = subprocess.run("./main",input = input_words[i], text=True, capture_output=True)
	n = i+1
	if res.stdout == results[i]:
		print("Test "+str(n)+ " succeed")
	else:
		print(f"Test "+str(n)+" failed. Result ("+res.stdout+") is not equal"+ results[i])
