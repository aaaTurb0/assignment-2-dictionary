%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%assign MAX_WORD_SIZE 256

section .rodata

not_found_message: db "value not found in dictionary", 0
length_error_message: db "input value length is up to 256", 0


section .bss
input_word: resb MAX_WORD_SIZE


section .text
global _start

print_and_exit:
    call print_string
    call print_newline
    jmp exit

_start:
    mov rdi, input_word
    mov rsi, MAX_WORD_SIZE
    push rdi
    push rsi
    call read_word
    pop rsi
    pop rdi
    test rax. rax
    je .too_long_input
    mov rdi, rax
    mov rsi, first_word 
    push rdi
    push rsi
    call find_word
    pop rsi
    pop rdi
    test rax, rax
    je .not_found
    add rdi, rax
    inc rdi
    jmp print_and_exit
    .not_found:
	    mov rdi, not_found_message
	    call print_and_exit
    .too_long_input:
	    mov rdi, length_error_message
	    call print_and_exit
