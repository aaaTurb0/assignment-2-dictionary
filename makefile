ASM=nasm
ASMFLAGS=-f elf64
LD=ld

main: main.o lib.o dict.o
	$(LD) -o main main.o lib.o dict.o

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o lib.o lib.asm
	
dict.o: dict.asm
	$(ASM) $(ASMFLAGS) -o dict.o dict.asm
	
main.o: main.asm colon.inc
	$(ASM) $(ASMFLAGS) -o main.o main.asm

clean:
	rm -rf *.o
	rm -rf main
